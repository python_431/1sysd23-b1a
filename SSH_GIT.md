# Configurer SSH et GIT

## SSH

- SSH (Secure Shell) permet de se connecter à distance à 
un système (UNIX/Linux principalement) avec chiffrement des
communication

- SSH permet à chaque utilisateur de s'authentifier avec
un système cryptographique à clef publique.

Principe : vous créez une paire de clef (information : des
nombres) l'une publique (PEUT être publiée sans risque) et
l'autre PRIVÉE (elle NE doit PAS être partagé avec qui que
ce soit). Si l'une des clefs est utilisée pour chiffrer 
quelque chose, c'est l'autre qui permet de déchiffrer.

GIT (et les plateforme GIT : gitlab et github) permet 
d'utiliser ces clef pour s'authentifier.

Étapes, dans un Terminal (sans être root) :

1. Gérer un couple de clefs privée/publique :

~~~~Bash
$ ls ~/.ssh
... voyez-vous des fichiers id_rsa et id_rsa.pub ? Normalement non.
... si oui : dites le moi !!!
$ ssh-keygen -t rsa
... à chaque question : répondez par [entrée]
$ ls ~/.ssh
id_rsa id_rsa.pub
~~~~

2. Si vous ne l'avez pas déjà fait créez un compte sur gitlab.com
   ou github.com
   (pour le groupe mettez votre identifiant ou test, pour le projet
   initial mettez test, peu import)

3. Créez un nouveau "project/repository" (blank) : nommez le "1sysd" et 
   sélectionnez "Public" !!! 

4. Allez sur votre projet, bouton "code", copiez-collez l'adresse
   de type https que vous voyez et postez là dans le chat WiMI

Pour pouvoir "cloner" votre propre dépôt sur un système où vous
avez créé une paire de clefs publique/privée il suffit de copier
dans le presse-papier la clef publique (ici tout ce qui va de
ssh-rsa jusqu'à john@bidule) :

~~~~Bash
$ cat ~/.ssh/id_rsa.pub
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDEV...
YFCpoNpicwb6wfE8RCYUqNY5wK1N8rpBwfZvOksF2...
GU1vOR2OsA5E3WMGeBrI/OWAV2RoCagkYNCh9GLNw...
...QCtF2r58GtoWDFDIK7x john@bidule
~~~~

Et de l'ajouter dans la section "SSH Keys" de votre profil
gitlab/github.

Installer GIT : 

- Sous Debian/Ubuntu/Mint/etc. : `sudo apt install git`
- Sous macOS : `brew install git`
- Sous MS Windows : https://git-scm.com/download/win

Testez que vous pouvez cloner votre dépôt avec son
adresse SSH (git@...) :

~~~~Bash
$ git clone git@.../1sysd.git
$ ls 
... 1sysd
~~~~


 

