#include<stdio.h>
#include<stdlib.h>
#include<time.h>

int main() {
    int numbers[10000];
    int repartition[10];

    // Remplit le tableau numbers
    // avec 10000 valeurs pseudo-aléatoire
    // entre 0 et 9 
    srand(time(NULL));
    for (int i = 0; i < 10000; i++) {
        numbers[i] = rand() % 10;
    } 
    // Calcule pour chaque nombre entre 1 et
    // et 10 combien de fois il a été tiré
    for (int i = 0; i < 10; i++) {
        repartition[i] = 0;
    }
    for (int i = 0; i < 10000; i++) {
        repartition[numbers[i]]++;
    }
    // Affiche les résultats
    for (int i = 0; i < 10; i++) {
        printf("%d : %d\n", i, repartition[i]);
    }

    exit(0);
}
