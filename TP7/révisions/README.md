# Révisions

## Fonctions, float et switch

Écrire deux fonctions `float euro2franc(float val)` et `float franc2euro(float val)` qui convertissent euros en francs (français) et inversement. Le taux de conversion est de 6,55957 francs pour un euro.

Dans la fonction `main` écrire le code pour que le programme se comporte
ainsi :

~~~~C
$ ./euros
(1) Franc vers Euro ou (2) Euro vers franc ? 3
(1) Franc vers Euro ou (2) Euro vers franc ? a
(1) Franc vers Euro ou (2) Euro vers franc ? 1
Valeur en Franc : 100
En Euro : 15.24
$ ./euros
(1) Franc vers Euro ou (2) Euro vers franc ? 2
Valeur en Euro : 15
En Francs : 98.39
$
~~~~

## Quelle est la "qualité" du hasard de srand/rand ?

Voir `TP1/game.c` pour retrouver comment tirer un nombre entier
au hasard.

Consignes en commentaire du fichier `testrandom.c`.

## snoisrevnI (Inversions)

Écrire `revert.c` afin que le programme revert affiche à l'envers
chacun des arguments qu'il reçoit :

~~~~Bash
$ ./revert hello goodbye
olleh
eybdoog
$ ./revert ""

$
~~~~

(Dans le second appel ici on passe une chaîne vide en premier argument !)

- Sans utiliser de fonctions de traitement de chaîne de la bibliothèque C
  (c'est-à-dire pas d'utilisation des fonctions de `string.h`)
- L'inversion se fera dans une fonction `char *reverse_str(char *s)` 
- On pourra supposer que les arguments font moins de cinquante caractères
  ou bien utiliser malloc pour allouer des chaînes de caractères de
  la bonne longueur
- Attention à bien traiter le cas d'une chaîne vide !

