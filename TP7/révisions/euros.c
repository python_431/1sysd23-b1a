#include<stdio.h>
#include<stdlib.h>

float euro2franc(float val) {
    return val * 6.55957;
}

float franc2euro(float val) {
    return val / 6.55957;
}

int main() {
    char ans;
    float val;
    
    ans = ' '; // valeur initiale invalide
    while (ans != '1' && ans != '2') {
        printf("(1) Franc vers Euro ou (2) Euro vers franc ? ");
        scanf(" %c", &ans);
    }
    switch (ans) {
        case '1':
            printf("Valeur en Franc : ");
            scanf("%f", &val);
            printf("En Euro : %.2f\n", franc2euro(val));
            break;
        case '2':
            printf("Valeur en Euro : ");
            scanf("%f", &val);
            printf("En Francs : %.2f\n", euro2franc(val));
            break;
    }
    exit(0);
}
