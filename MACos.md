# Faire les TP 1SYSD nativement sur Mac

- Allez sur https://brew.sh/ (brew est un outil en ligne
 de commande qui permet d'installer facilement 99% des
 outils "classique" en mode Terminal du "monde Linux"
 - GNU, etc - sans interférer avec l'écosystème Apple)

- Terminal.app fournit par Apple est pas trop mal mais
(selon certain) iTerm est mieux

Une fois ceci fait : la commande `brew install ...` peut
installer ce dont on a besoin, ex : `brew install git` 

