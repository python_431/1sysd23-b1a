## Un jeu simple : deviner un nombre

Écrire un programme C (game.c) similaire au programme Python game.py

## Une calculatrice simple

Écrire un programme C (calc.c) qui demande la saisie de deux nombres
flottants (float ou double) et un caractère parmi : `+`, `-`, `*`, `/` et
qui réalise l'opération correspondante et affiche le résultat.

Pour la saisie d'un caractère on procède ainsi :

~~~~C
char c;


printf("Opération : +, -, *, / : ");
// notez l'espace avant le %c
scanf(" %c", &c);
~~~~

Note : attention avec scanf : si vous choisissez le type `float` le
format pour scanf est `"%f"`, si vous choisissez le type `double` le
format est `"%lf"`.
