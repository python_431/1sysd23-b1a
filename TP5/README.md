# Quizz : fichier `quizz.c`

Que font les fonctions `quizz1` et `quizz2` ?

Premier point : Si `p` est un pointeur vers un caractère
(`char *p`) qui, initialement, pointe vers le début
d'une chaîne de caractère, i.e.

~~~~
p -----> |H|e|l|l|o| |W|o|r|l|d|0|
~~~~
(c'est vraiment 0 = _zéro_ en dernier !)

Que _valent_ et que _font_ respectivement :

- `*p`
- `*p++`
- `*++p` 

Quand `*p++` est-il nul ?

## Un classique : identifier les palindromes

Un mot est un palindrome s'il se lit de la même façon à l'endroit
et à l'envers de façon optimale (minimum de comparaison de caractères).

Fichier de départ : palindrome.c.
