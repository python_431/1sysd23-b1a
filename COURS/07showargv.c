#include<stdio.h>
#include<stdlib.h>

int main(int argc, char *argv[]) {
    // argc = argument count ; combien d'argument j'ai reçu
    // argv = arguments values ; quels sont-ils ?
    // argv[0] : nom du programme exécuté
    // argv[1] : premier argument (chaîne),
    // argv[2] : deuxième argument (chaîne) etc.
    // argv[3] : argv[3][0] : premier caractère du 3ème argument

    for (int i = 0; i < argc; i++) {
        printf("Argument %d : %s\n", i, argv[i]);
    }

    exit(0);

}
