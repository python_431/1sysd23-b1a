# Notre premier programme C

- Vérifiez que GIT est bien installé :

  - GNU/Linux de type Debian/Ubuntu/Mint/... : `sudo apt install git`
  - macOS : `sudo brew install git`

- Si vous ne l'avez pas déjà fait clonez votre dépôt GIT,
récupérer dans le presse-papier son url (lien) SSH qui
est de la forme "git@.../1sysd" et dans un terminal 

- GIT veut savoir qui vous êtes pour l'indiquer dans les
information d'historique du projet, à faire une fois :

~~~~Bash
$ git config --global user.email "vous@peutimporte"
$ git config --global user.name "votre prénom"
~~~~

- Vous devrez voir que votre est bien là (un répertoire nommé 1sysd) :

~~~~Bash
$ ls
... 1sysd ...
~~~~

- On "va" dedans : `cd 1sysd`
- On peut vérifier "où" on est : `pwd` (montrer `/home/votre_id/1sysd`)
- On peut y créer un répertoire : `mkdir TP0`
- On peut y aller : `cd TP0`

On va créer notre premier programme C : `hello.c` :

Quel éditeur utiliser :

- En mode terminal : `nano` (facile), `vim` (`sudo apt install vim`),
  on a un bureau graphique : `gedit` (`sudo apt install gedit`)

~~~~Bash
$ nano hello.c # si vous voulez utiliser nano
$ gedit hello.c & # si vous voulez utiliser gedit
~~~~

Il y a de nombreux éditeurs orienté développement C (et autre
langages) : vim, emacs, vscode (bof), etc. etc.

Premier source en C :

~~~~C
#include<stdio.h>
#include<stdlib.h>

int main() {
    printf("Hello World!\n");
    exit(0);
}
~~~~

Enregistrer le sous le nom `hello.c` et dans un Shell (Terminal) :

~~~~Bash
$ cc hello.c
$ ls
a.out hello.c
$ ./a.out
Hello World!
$ 
~~~~

Si tout va bien on peut ajouter ce fichier `hello.c` à notre
dépôt, "committer" (enregistrer) sont état actuel et "pousser"
la version de notre projet sur gitlab/github :

~~~~Bash
$ git add hello.c
$ git commit -a -m "first C program"
$ git push
~~~~

Actualisez dans votre navigateur la page Web de votre projet,
vous devez y voir le répertoire TP0 et dans celui-ci le fichier
`hello.c`.

## Plus de détail

Les lignes du source qui débutent par `#` sont traité par le
préprocesseur, par exemple `#include` déclenche l'insertion
du contenu du fichier indiqué. `stdio.h` : fichier d'en-tête,
qui contient la définition de la fonction `printf`.

## À vous !

Créez un nouveau fichier source C nommé `bonjour.c` qui
fait la même chose que `hello.c` mais en français. Ajoutez
une cible/dépendance et une action dans le Makefile pour que
ceci fonctionne :

~~~~Bash
$ pwd
.../sysd/TP0
$ ls
... bonjour.c  ... Makefile ...
$ make bonjour
gcc -o bonjour bonjour.c
$ ls 
... bonjour ... bonjour.c Makefile
$ ./bonjour
Bonjour tout le monde !
$ 
~~~~

Une fois que ça fonctionne : ajoutez `bonjour.c` et `Makefile` à votre
dépôt GIT et publiez la version à jour de votre dépôt :

~~~~Bash
$ git add Makefile bonjour.c
$ git commit -a -m "TP0 terminé"
$ git push
~~~~

Vérifiez sur gitlab/github que votre dépôt est à jour.

## Note pour la suite

Vous pouvez cloner mon dépôt et vous pourrez le mettre à
jour au fur et à mesure que je publierai exemples et
solutions de TPs :

~~~~Bash
$ cd
$ pwd
/home/votre_id
$ git clone https://gitlab.com/python_431/1sysd23-b1a.git
$ ls
... 1sysd ... 1sysd23-b1a ...
~~~~

Le répertoire `1sysd` est _votre_ dépôt où vous aller travailler,
le répertoire `1sysd23-b1a` est mon dépôt, une fois qu'il a été
cloné vous pouvez le mettre à jour facilement :

~~~~Bash
$ cd 
$ cd 1sysd23-b1a
$ pwd
.../1sysd23-b1a
$ git pull
~~~~



## Ref.

Le C en 20 heures : https://archives.framabook.org/le-c-en-20-heures-2/
